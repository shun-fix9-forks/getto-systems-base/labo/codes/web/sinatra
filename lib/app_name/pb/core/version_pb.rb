# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: app_name/pb/core/version.proto

require 'google/protobuf'

Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("app_name/pb/core/version.proto", :syntax => :proto3) do
    add_message "getto_codes.app_name.pb.core.version.Request" do
    end
    add_message "getto_codes.app_name.pb.core.version.Response" do
      optional :core, :string, 1
      optional :repo, :string, 2
    end
  end
end

module GettoCodes
  module AppName
    module Pb
      module Core
        module Version
          Request = Google::Protobuf::DescriptorPool.generated_pool.lookup("getto_codes.app_name.pb.core.version.Request").msgclass
          Response = Google::Protobuf::DescriptorPool.generated_pool.lookup("getto_codes.app_name.pb.core.version.Response").msgclass
        end
      end
    end
  end
end
