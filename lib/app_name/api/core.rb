require "grpc"
require "app_name/pb/core_services_pb"

module GettoCodes
  module AppName
    module Api
      class Core
        def initialize(host)
          @stub = Pb::Core::Stub.new host, :this_channel_is_insecure
        end

        attr_reader :stub

        def version
          res = stub.version(Pb::Core::Version::Request.new)
          {
            core: res.core,
            repo: res.repo,
          }
        end
      end
    end
  end
end
